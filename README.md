Oct is a portable Lisp implementation of quad-double arithmetic. This
gives about 65 digits of precision. Quad-double arithmetic uses four
double-float numbers to represent an extended precision number.

The implementation is modeled on the [quad-double package](https://www.davidhbailey.com/dhbpapers/quad-double.pdf) by Yozo
Hida. This package is in C++, but we have translated parts of it and
extended it to use Lisp. The intent is to provide all of the CL
arithmetic functions with a quad-double implementation.

See the [wiki](https://gitlab.common-lisp.net/oct/oct/-/wikis/home) for more information.
