;;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
(in-package :asdf)

(defsystem :rt
  :name "rt"
  :licence "MIT"
  :description "MIT Regression Tester"
  :long-description "RT provides a framework for writing regression test suites"
  :perform (load-op :after (op rt)
		    (pushnew :rt cl:*features*))
  :components
  ((:file "rt-package")
   (:file "rt")))
